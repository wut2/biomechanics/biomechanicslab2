function [a1, a2_a, a2_b, a2_c, a2_d, a3_a, a3_b, a3_c, a3_d, a4] = readXls()
a1 = xlsread('lab1.xlsx', 'TABLE A.1');
a2_a = xlsread('lab1.xlsx', 'TABLE A.2a');
a2_b = xlsread('lab1.xlsx', 'TABLE A.2b');
a2_c = xlsread('lab1.xlsx', 'TABLE A.2c');
a2_d = xlsread('lab1.xlsx', 'TABLE A.2d');
a3_a = xlsread('lab1.xlsx', 'TABLE A.3a');
a3_b = xlsread('lab1.xlsx', 'TABLE A.3b');
a3_c = xlsread('lab1.xlsx', 'TABLE A.3c');
a3_d = xlsread('lab1.xlsx', 'TABLE A.3d');
a4 = xlsread('lab1.xlsx', 'TABLE A.4');
end