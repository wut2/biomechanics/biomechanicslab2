function acc = computeAcc(pos1, pos2, t1, t2)

% function to calculate velocity --> (x(i)-x(i-1)/ t(i)-t(i-1))
time_change = t2 - t1;
vel_change  = pos2 - pos1;
acc = vel_change / time_change;

end