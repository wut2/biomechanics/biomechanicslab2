Make sure to compile bib file:
- Compile two times using "pdfLatex"
- Compile Biber
- Compile again pdfLatex

# Remark
In order to generate the section "references", a `\cite` command needs to exist somewhere in the project.